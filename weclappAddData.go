package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/go-resty/resty/v2"
	"github.com/spf13/viper"
)

// authToken needed

var client *resty.Client = resty.New()
var authToken string
var weclappBaseUrl string
var urlUser string
var urlSalesOrder string
var urlTasks string
var urlTimeRecord string

var helpFlag *bool
var jsonFlag *string
var fullSyncFlag *bool
var incSyncFlag *bool

type User struct {
	FirstName string        `json:"firstName"`
	LastName  string        `json:"lastName"`
	Email     string        `json:"email"`
	Status    string        `json:"status"`
	UserRoles []interface{} `json:"userRoles"`
}

type TimeRecord struct {
	TaskId      string `json:"taskId"`
	UserId      string `json:"userId"`
	StartDate   int    `json:"startDate"`
	Duration    int    `json:"durationSeconds"`
	Description string `json:"description"`
}

func main() {
	loadCredentials()
	getCommandLineFlags()

	client.Header.Set("AuthenticationToken", authToken)
	data := loadJson(*jsonFlag)

	weclappAddData(data)

}

func loadCredentials() {
	viper.SetConfigFile("weclappCredentials.yaml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig() // Find and read the config file
	if err != nil {             // Handle errors reading the config file
		panic(fmt.Errorf("fatal error config file: %w", err))
	}
	authToken = viper.GetString("authToken")
	weclappBaseUrl = viper.GetString("weclappBaseUrl")
	urlUser = weclappBaseUrl + "/webapp/api/v1/user"
	urlSalesOrder = weclappBaseUrl + "/webapp/api/v1/salesOrder"
	urlTasks = weclappBaseUrl + "/webapp/api/v1/task"
	urlTimeRecord = weclappBaseUrl + "/webapp/api/v1/timeRecord"
}

func loadJson(jsonFile string) []map[string]interface{} {
	body, err := os.ReadFile(jsonFile)
	if err != nil {
		panic(err)
	}
	var data []map[string]interface{}
	_ = json.Unmarshal(body, &data)
	return data
}

func getCommandLineFlags() {
	helpFlag = flag.Bool("help", false, "Help information")
	jsonFlag = flag.String("json", "", "Path to JSON file")
	fullSyncFlag = flag.Bool("fullSync", false, "Perform a full sync")
	incSyncFlag = flag.Bool("incSync", false, "Perform an inc sync")
	flag.Parse()

	if *helpFlag || (*jsonFlag == "") {
		fmt.Println(`
usage: weclappAddData.go [--help] --json JSON [--fullSync] [--incSync]

Add Data to weclapp

options:
--help   show this help message and exit
--json JSON  JSON-filepath which should be used
--full_sync  Deletes timeRecords before adding
--inc_sync   Update duration of timeRecords if the rest is the same
		`)
		os.Exit(0)
	}
}

// ----------------------CREATE OR UPDATE USER--------------------------------
func updateUser(user User, raw map[string]interface{}) {
	userToUpdate := User{
		FirstName: user.FirstName,
		LastName:  user.LastName,
		Email:     user.Email,
		Status:    raw["status"].(string),
		UserRoles: raw["userRoles"].([]interface{}),
	}

	url := urlUser + "/id/" + raw["id"].(string)

	resp, _ := client.R().SetHeader("Content-Type", "application/json").SetBody(userToUpdate).Put(url)
	if resp.StatusCode() == 200 {
		fmt.Printf("Successfully updated username '%v %v'\n", user.FirstName, user.LastName)
	} else {
		fmt.Printf("Unsuccesful updating username '%v %v'\n", user.FirstName, user.LastName)
	}
}

func createOrUpdateUser(user User) {
	resp, _ := client.R().SetHeader("Content-Type", "application/json").
		SetBody(user).Post(urlUser)
	if resp.StatusCode() == 201 {
		fmt.Printf("Successfully created user '%s' on weclapp!\n", user.Email)
	} else if resp.StatusCode() == 400 {
		fmt.Printf("Email address '%s' already in use.\n", user.Email)
		resp, _ := client.R().SetHeader("Content-Type", "application/json").Get(urlUser)

		var users map[string][]map[string]interface{}
		json.Unmarshal(resp.Body(), &users)

		for _, raw := range users["result"] {
			if raw["email"] == user.Email && (raw["firstName"] != user.FirstName || raw["lastName"] != user.LastName) {
				updateUser(user, raw)
			}
		}
	} else {
		fmt.Printf("Error (User-creation failed): %d\n", resp.StatusCode())
	}
}

func createOrUpdateUsers(data []map[string]interface{}) {

	users := make(map[string]User)

	for _, raw := range data {
		name := strings.SplitN(raw["user_name"].(string), " ", 2)
		firstName, lastName := name[0], name[1]
		email := raw["user_mail"].(string)
		user := User{
			FirstName: firstName,
			LastName:  lastName,
			Email:     email,
			Status:    "ACTIVE",
			UserRoles: make([]interface{}, 0),
		}
		users[email] = user
	}
	for _, user := range users {
		createOrUpdateUser(user)
	}
}

// -------------------------CREATE TIME RECORD---------------------------

func retrieveUserByEmail(email string) map[string]interface{} {
	resp, _ := client.R().SetHeader("Content-Type", "application/json").Get(urlUser)
	var users map[string][]map[string]interface{}
	json.Unmarshal(resp.Body(), &users)
	for _, raw := range users["result"] {
		if raw["email"] == email {
			return raw
		}
	}
	panic(fmt.Sprintf("No user with email '%v'", email))
}

func retrieveSalesOrderByOrderNumber(orderNumber string) map[string]interface{} {
	resp, _ := client.R().SetHeader("Content-Type", "application/json").Get(urlSalesOrder)

	var salesOrders map[string][]map[string]interface{}
	json.Unmarshal(resp.Body(), &salesOrders)
	for _, raw := range salesOrders["result"] {
		if raw["orderNumber"] == orderNumber {
			return raw
		}
	}
	panic(fmt.Sprintf("No sales order with number '%v'", orderNumber))
}

func retrieveTasks() map[string]map[string]interface{} {
	resp, _ := client.R().SetHeader("Content-Type", "application/json").Get(urlTasks)
	var tasks map[string][]map[string]interface{}
	json.Unmarshal(resp.Body(), &tasks)
	var res = make(map[string]map[string]interface{}, 0)
	for _, raw := range tasks["result"] {
		res[raw["id"].(string)] = raw
	}
	return res
}

func ensureAssignee(task map[string]interface{}, userId string) {
	assigneeExists := false
	for _, assignee := range task["assignees"].([]interface{}) {
		if assignee.(map[string]interface{})["userId"].(string) == userId {
			assigneeExists = true
			break
		}
	}
	if !assigneeExists {
		newAssignee := map[string]interface{}{
			"userId": userId,
		}
		task["assignees"] = append(task["assignees"].([]interface{}), newAssignee)
		url := urlTasks + "/id/" + task["id"].(string)
		_, err := client.R().SetHeader("Content-Type", "application/json").SetBody(task).Put(url)
		if err != nil {
			fmt.Println("Appending assignee failed")
		}
	}
}

func createTimeRecord(orderNumber string, position int, userEmail string, startDate, duration int, description string) {
	user := retrieveUserByEmail(userEmail)
	userId := user["id"].(string)
	salesOrder := retrieveSalesOrderByOrderNumber(orderNumber)
	tasks := retrieveTasks()

	for _, orderItem := range salesOrder["orderItems"].([]interface{}) {
		if int(orderItem.(map[string]interface{})["positionNumber"].(float64)) == position {
			// Es gibt in der Regel nur einen Task pro Position --> [0]
			taskId := orderItem.(map[string]interface{})["tasks"].([]interface{})[0].(map[string]interface{})["id"].(string)
			task := tasks[taskId]
			ensureAssignee(task, userId)

			newTimeRecord := TimeRecord{
				TaskId:      taskId,
				UserId:      userId,
				StartDate:   startDate,
				Duration:    duration,
				Description: description,
			}
			resp, _ := client.R().SetHeader("Content-Type", "application/json").SetBody(newTimeRecord).Post(urlTimeRecord)
			if resp.StatusCode() == 201 {
				fmt.Printf("Successfully added timeRecord '%v'\n", description)
			} else {
				fmt.Printf("Unsuccessful: '%v' Status-Code: %v\n", description, resp.StatusCode())
			}
			return
		}
	}
}

// -------------------------DELETE TIMERECORD----------------------------
func retrieveTimeRecords() map[string]map[string]interface{} {
	resp, _ := client.R().SetHeader("Content-Type", "application/json").Get(urlTimeRecord)

	var raw map[string][]map[string]interface{}

	json.Unmarshal(resp.Body(), &raw)

	var timeRecords = make(map[string]map[string]interface{})

	for _, timeRecord := range raw["result"] {
		timeRecords[timeRecord["id"].(string)] = timeRecord
	}

	return timeRecords
}

func deleteAllTimeRecordsFromTodateFromJson(data []map[string]interface{}) {
	firstDate, lastDate := math.MaxInt, 0
	for _, raw := range data {
		firstDate = min(firstDate, int(raw["day"].(float64)))
		lastDate = max(lastDate, int(raw["day"].(float64)))
	}
	firstDate += 32400000
	lastDate += 32400000

	timeRecords := retrieveTimeRecords()
	for _, timeRecord := range timeRecords {
		date := int(timeRecord["startDate"].(float64))
		if firstDate <= date && date <= lastDate {
			url := urlTimeRecord + "/id/" + timeRecord["id"].(string)
			resp, _ := client.R().SetHeader("Content-Type", "application/json").Delete(url)
			seconds := int64(date / 1000)
			timeStamp := time.Unix(seconds, 0)
			formattedTime := timeStamp.Format("2006-01-02 15:04")
			if resp.StatusCode() == 204 {
				fmt.Printf("Successfully deleted timeRecord on date '%v'\n", formattedTime)
			} else {
				fmt.Printf("deletion unsuccessful of timeRecord on date '%v'\n", formattedTime)
			}
		}
	}
}

//------------------------------UPDATE TIME RECORD---------------------------------------

func updateTimeRecord(timeRecord map[string]interface{}, newDuration int) {
	if int(timeRecord["durationSeconds"].(float64)) != newDuration {
		timeRecord["durationSeconds"] = newDuration
		url := urlTimeRecord + "/id/" + timeRecord["id"].(string)
		resp, _ := client.R().SetHeader("Content-Type", "application/json").SetBody(timeRecord).Put(url)
		if resp.StatusCode() == 200 {
			fmt.Printf("Successfully updated timeRecord '%v'\n", timeRecord["description"])
		} else {
			fmt.Printf("Update unsuccessful of timeRecord '%v'\n", timeRecord["description"])
		}
	}
}

// ------------------------------CHECK TIME RECORD EXISTS?-------------------
func doesTimeRecordExist(orderNumber string, position int, userMail string, startDate int, description string) map[string]interface{} {
	timeRecords := retrieveTimeRecords()
	salesOrder := retrieveSalesOrderByOrderNumber(orderNumber)
	for _, timeRecord := range timeRecords {

		if int(timeRecord["startDate"].(float64)) == startDate && timeRecord["userUsername"] == userMail && timeRecord["description"] == description {
			taskId := timeRecord["taskId"]
			for _, orderItem := range salesOrder["orderItems"].([]interface{}) {
				if int(orderItem.(map[string]interface{})["positionNumber"].(float64)) == position {
					for _, task := range orderItem.(map[string]interface{})["tasks"].([]interface{}) {
						if task.(map[string]interface{})["id"].(string) == taskId {
							return timeRecord
						}
					}
				}
			}
		}
	}
	return nil
}

//------------------------------SYNC FORMS----------------------------------

func fullSync(data []map[string]interface{}) {
	// Alle Zeitbuchungen im Zeitraum von data löschen
	deleteAllTimeRecordsFromTodateFromJson(data)
	// Alle vorhandenen Benutzer in Json ermitteln und in weclapp anlegen/updaten
	createOrUpdateUsers(data)

	for _, timeRecord := range data {
		startDate := int(timeRecord["day"].(float64)) + 32400000 // 32400000 ms = 9 hours
		erpLink := strings.Split(timeRecord["erp_link"].(string), "-")
		orderNumber := erpLink[0]
		position, _ := strconv.Atoi(erpLink[1])
		email := timeRecord["user_mail"].(string)
		description := timeRecord["output_summary"].(string)
		duration := int(timeRecord["duration"].(float64)) / 1000

		createTimeRecord(orderNumber, position, email, startDate, duration, description)
	}
}

func incSync(data []map[string]interface{}) {
	createOrUpdateUsers(data)
	for _, timeRecord := range data {
		startDate := int(timeRecord["day"].(float64)) + 32400000 // 32400000 ms = 9 hours
		erpLink := strings.Split(timeRecord["erp_link"].(string), "-")
		orderNumber := erpLink[0]
		position, _ := strconv.Atoi(erpLink[1])
		email := timeRecord["user_mail"].(string)
		description := timeRecord["output_summary"].(string)
		duration := int(timeRecord["duration"].(float64)) / 1000
		if timeRecord := doesTimeRecordExist(orderNumber, position, email, startDate, description); timeRecord != nil {
			updateTimeRecord(timeRecord, duration)
		} else {
			createTimeRecord(orderNumber, position, email, startDate, duration, description)
		}
	}
}

func weclappAddData(data []map[string]interface{}) {
	if *fullSyncFlag && *incSyncFlag {
		fmt.Println("Choose only one Sync (--fullSync or --incSync)")
		return
	} else if *fullSyncFlag {
		fmt.Println("Performing full sync ...")
		fullSync(data)
	} else if *incSyncFlag {
		fmt.Println("Performing inc sync ...")
		incSync(data)
	} else {
		fmt.Println("Choose one Sync (--fullSync or --incSync)")
	}
}
