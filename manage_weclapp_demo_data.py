#!/usr/bin/env python3

import requests
import json
from dataclasses import dataclass
import argparse

from weclapp_credentials import *


parser = argparse.ArgumentParser(
    description="Create and delete Demo-data on weclapp (customers, articles, salesOrders)",
    epilog="""
    ***Necessary Variables:
        Set Authentication-Token as Environment-Variable
"""
)

parser.add_argument("--create",
                    action="store_true",
                    help="Create 5 Customers, 2 Articles and 5 SalesOrders"
)

parser.add_argument("--delete",
                    action="store_true",
                    help="Delete 5 Customers, 2 Articles and 5 SalesOrders"
)

args = parser.parse_args()

headers = {
    "AuthenticationToken": auth_token,
    "Content-Type": "application/json"
}

customer_numbers = ["90000", "90001", "90002", "90003", "90004"]
article_numbers = ["777", "888"]
order_numbers = ["9000", "9001", "9002", "9003", "9004"]

@dataclass(frozen=True)
class Demo_data:
    customers: list
    articles: list
    sales_orders: list

demo_Data = Demo_data(
# Customers
[{ # Customer 1
    "company": "Mustermann AG",
    "partyType": "ORGANIZATION",
    "email": "mustermann.ag@gmail.com",
    "city": "Stuttgart",
    "phone": "12345-1234567",
    "customerNumber": customer_numbers[0]
},
{ # Customer 2
    "company": "Christus GmbH",
    "partyType": "ORGANIZATION",
    "email": "christus@gmail.com",
    "city": "Bethlehem",
    "phone": "12345-1234567",
    "customerNumber": customer_numbers[1]
},
{ # Customer 3
    "company": "LALA Firma",
    "partyType": "ORGANIZATION",
    "email": "lalafirma@gmail.com",
    "city": "München",
    "phone": "12345-1234567",
    "customerNumber": customer_numbers[2]
},
{ # Customer 4
    "company": "Jaguar Firma",
    "partyType": "ORGANIZATION",
    "email": "jaguar.firma@gmail.com",
    "city": "Hamburg",
    "phone": "12345-1234567",
    "customerNumber": customer_numbers[3]
},
{ # Customer 5
    "company": "The boring company",
    "partyType": "ORGANIZATION",
    "email": "theboringcompany@gmail.com",
    "city": "Los Angeles",
    "phone": "12345-1234567",
    "customerNumber": customer_numbers[4]
},
], [ # Articles
    { # Article 1
    "name": "Magischer Trank",
    "articleNumber": article_numbers[0],
    "unitName": "Stk.",
    "articleType": "STORABLE",
    "articlePrices": [
        {
            "currencyName": "EUR",
            "priceScaleType": "SCALE_FROM",
            "priceScaleValue": "0",
            "salesChannel": "NET1",
            "price": "127"
        }
    ]
}, { # Article 2
    "name": "Softwareentwicklung",
    "articleNumber": article_numbers[1],
    "unitName": "h",
    "articleType": "SERVICE",
    "plannedWorkingTimePerUnit": 3600,
    "invoicingType": "EFFORT",
    "articlePrices": [
        {
            "currencyName": "EUR",
            "priceScaleType": "SCALE_FROM",
            "priceScaleValue": "0",
            "salesChannel": "NET1",
            "price": "64"
        }
    ]
}
], [ # SalesOrders
    { # SalesOrder 1
    "customerNumber": customer_numbers[0],
    "orderNumber": order_numbers[0],
    "status": "ORDER_CONFIRMATION_PRINTED",
    "orderItems": [
        {
            "articleNumber": "777"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "7"
        }
    ]
}, { # SalesOrder 2
    "customerNumber": customer_numbers[1],
    "orderNumber": order_numbers[1],
    "status": "ORDER_CONFIRMATION_PRINTED",
    "orderItems": [
        {
            "articleNumber": "777"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "1"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "2"
        }
    ]
}, { # SalesOrder 3
    "customerNumber": customer_numbers[2],
    "orderNumber": order_numbers[2],
    "status": "ORDER_CONFIRMATION_PRINTED",
    "orderItems": [
        {
            "articleNumber": "777"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "1"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "2"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "3"
        }
    ]
}, { # SalesOrder 4
    "customerNumber": customer_numbers[3],
    "orderNumber": order_numbers[3],
    "status": "ORDER_CONFIRMATION_PRINTED",
    "orderItems": [
        {
            "articleNumber": "777"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "1"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "2"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "3"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "4"
        }
    ]
},
{    # SalesOrder 5
    "customerNumber": customer_numbers[4],
    "orderNumber": order_numbers[4],
    "status": "ORDER_CONFIRMATION_PRINTED",
    "orderItems": [
        {
            "articleNumber": "777"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "1"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "2"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "3"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "4"
        },
        {
            "articleNumber": "888",
            "invoicingType": "EFFORT",
            "serviceItem": True,
            "quantity": "5"
        }
    ]
}
])

def create_demo_data():
    # create Customers
    for i in range(5):
        response = requests.post(url_customer, headers=headers, data=json.dumps(demo_Data.customers[i]))
        if response.status_code == 201:
            print("Successfully created customer")
    # create Articles
    for i in range(2):
        response = requests.post(url_article, headers=headers, data=json.dumps(demo_Data.articles[i]))
        if response.status_code == 201:
            print("Successfully created article")
    # create SalesOrders
    for i in range(5):
        response = requests.post(url_salesOrder, headers=headers, data=json.dumps(demo_Data.sales_orders[i]))
        if response.status_code == 201:
            print("Successfully create salesOrder")

def delete_demo_data():
    response_customers = requests.get(url_customer, headers=headers)
    response_articles = requests.get(url_article, headers=headers)
    response_sales_orders = requests.get(url_salesOrder, headers=headers)

    for raw in response_sales_orders.json()["result"]:
        for order_number in order_numbers:
            if raw["orderNumber"] == order_number:
                response = requests.delete(f"{url_salesOrder}/id/{raw['id']}", headers=headers)
                if response.status_code == 204:
                    print("Successfully deleted salesOrder")

    for raw in response_articles.json()["result"]:
        for article_number in article_numbers:
            if raw["articleNumber"] == article_number:
                response = requests.delete(f"{url_article}/id/{raw['id']}", headers=headers)
                if response.status_code == 204:
                    print("Successfully deleted article")

    for raw in response_customers.json()["result"]:
        for customer_number in customer_numbers:
            if raw["customerNumber"] == customer_number:
                response = requests.delete(f"{url_customer}/id/{raw['id']}", headers=headers)
                if response.status_code == 204:
                    print("Successfully deleted customer")


if args.create and args.delete:
    print("Choose one: --create or --delete")
elif args.create:
    create_demo_data()
elif args.delete:
    delete_demo_data()
else:
    print("Choose one: --create or --delete")