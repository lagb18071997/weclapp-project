FROM python:3.10-slim-bullseye

COPY ./requirements.txt /run/requirements.txt

COPY . .

RUN set -eux; \
    export LANG=C.UTF-8; \
    pip3 install --upgrade -r /run/requirements.txt mypy pdoc; \
    rm /run/requirements.txt

CMD ["python3"]