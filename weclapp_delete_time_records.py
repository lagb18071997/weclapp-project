#!/usr/bin/env python3

import argparse

import requests
from datetime import datetime

from weclapp_credentials import *

parser = argparse.ArgumentParser(
    description="Delete timeRecord by Date **OR** by Month",
    epilog="""
    ***Necessary Variables:
        Set Authentication-Token as Environment-Variable
"""
)

parser.add_argument("--date",
                    type=str,
                    help="Date in ISO-Format %%Y-%%m-%%d (e.g. 2024-02-13)")
parser.add_argument("--month",
                    type=str,
                    help="Month as a Number between 1 and 12")

args = parser.parse_args()


headers = {
    "AuthenticationToken": auth_token,
    "Content-Type": "application/json"
}

def retrieve_time_records():
    response = requests.get(url_timeRecord, headers=headers)
    time_records = {}
    for raw in response.json()["result"]:
        time_records[raw["id"]] = raw
    return time_records

def delete_all_time_records_by_month(month):
    time_records = retrieve_time_records()
    for time_record in time_records.values():
        month_of_time_record = datetime.utcfromtimestamp(time_record["startDate"]/1000).strftime("%m")
        if int(month_of_time_record) == int(month):
            response = requests.delete(f"{url_timeRecord}/id/{time_record['id']}", headers=headers)
            if response.status_code == 204:
                print(f"Successfully deleted timeRecord on date {datetime.utcfromtimestamp(time_record['startDate']/1000).strftime('%Y-%m-%d %H:%M')}")
            else:
                print(f"Deletion unsuccessful of timeRecord on date {datetime.utcfromtimestamp(time_record['startDate']/1000).strftime('%Y-%m-%d %H:%M')}")

def delete_all_time_records_by_date(date):
    time_records = retrieve_time_records()
    for time_record in time_records.values():
        start_date_of_time_record = datetime.utcfromtimestamp(time_record["startDate"]/1000).strftime("%Y-%m-%d")
        if start_date_of_time_record == date:
            response = requests.delete(f"{url_timeRecord}/id/{time_record['id']}", headers=headers)
            if response.status_code == 204:
                print(f"Successfully deleted timeRecord on date {datetime.utcfromtimestamp(time_record['startDate']/1000).strftime('%Y-%m-%d %H:%M')}")
            else:
                print(f"Deletion unsuccessful of timeRecord on date {datetime.utcfromtimestamp(time_record['startDate']/1000).strftime('%Y-%m-%d %H:%M')}")


if args.date and args.month:
    print("Choose only ONE! Date (e.g. 2024-02-13) **OR** Month (1 - 12)")
elif args.date:
    delete_all_time_records_by_date(args.date)
elif args.month:
    delete_all_time_records_by_month(args.month)
else:
    print("Choose ONE! Date (e.g. 2024-02-13) **OR** Month (1 - 12)")