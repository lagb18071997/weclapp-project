#!/usr/bin/env python3

import requests
import json

from datetime import datetime

import argparse

from weclapp_credentials import *

parser = argparse.ArgumentParser(
    description="Create timeRecords on weclapp via CLI",
    epilog="""
    ***Necessary Variables:
        Set Authentication-Token as Environment-Variable
"""
)

parser.add_argument("--order",
                    type=str,
                    help="orderNumber of SalesOrder",
                    required=True)
parser.add_argument("--pos",
                    type=int,
                    help="Positionnumber of orderItem",
                    required=True)
parser.add_argument("--user-mail",
                    type=str,
                    help="E-Mail of user",
                    required=True)
parser.add_argument("--datetime",
                    type=str,
                    help="Starttime in ISO-Format %%Y-%%m-%%d %%H:%%M (e.g. 2024-02-13 10:15)",
                    required=True)
parser.add_argument("--duration",
                    type=int,
                    help="duration in MINUTES of timeRecord",
                    required=True)

parser.add_argument("--description",
                    type=str,
                    help="Description of timeRecord")

args = parser.parse_args()

headers = {
    "AuthenticationToken": auth_token,
    "Content-Type": "application/json"
}

def retrieve_user_by_email(email):
    response = requests.get(url_user, headers=headers)
    for raw in response.json()["result"]:
        if email == raw["email"]:
            return raw
    raise Exception(f'No user with email {email}!')    

def retrieve_sales_order_by_order_number(order_number):
    response = requests.get(url_salesOrder, headers=headers)
    for raw in response.json()["result"]:
        if order_number == raw["orderNumber"]:
            return raw
    raise Exception(f'No sales order with number {order_number}!')

def retrieve_tasks():
    response = requests.get(url_tasks, headers=headers)
    tasks = {}
    for raw in response.json()["result"]:
        tasks[raw["id"]] = raw
    return tasks


def ensure_assignee(task, user_id):
    assignee_exists = False
    for assignee in task["assignees"]:
        if user_id == assignee["userId"]:
            assignee_exists = True
    if not assignee_exists:
        new_assignee = {"userId": user_id}
        task["assignees"].append(new_assignee)
        response = requests.put(f"{url_tasks}/id/{task['id']}", headers=headers, data=json.dumps(task))

def create_time_record_with_all_args(order_number, pos, user_email, start_date, duration, description):
    user = retrieve_user_by_email(user_email)
    user_id = user["id"]
    sales_order = retrieve_sales_order_by_order_number(order_number)
    tasks = retrieve_tasks()

    for order_item in sales_order["orderItems"]:
        if order_item["positionNumber"] == pos:
            task_id = order_item["tasks"][0]["id"] # es gibt in der Regel nur einen Task pro Auftrag
            task = tasks[task_id]
            ensure_assignee(task, user_id)

            new_time_record = {
                "taskId": task_id,
                "userId": user_id,
                "startDate": int(datetime.strptime(start_date, "%Y-%m-%d %H:%M").timestamp() * 1000),
                "durationSeconds": duration * 60,
                "description": description
            }

            response = requests.post(url_timeRecord, headers=headers, data=json.dumps(new_time_record))
            print(response.text)
            if response.status_code == 201:
                print("Successfully added timeRecord!")
            return


create_time_record_with_all_args(args.order, args.pos, args.user_mail, args.datetime, args.duration, args.description)