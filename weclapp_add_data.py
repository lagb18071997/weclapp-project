#!/usr/bin/env python3

import argparse
import json
import requests
from datetime import datetime

from weclapp_credentials import *

url_user = weclapp_base_url + "/webapp/api/v1/user"
url_salesOrder = weclapp_base_url + "/webapp/api/v1/salesOrder"
url_tasks = weclapp_base_url + "/webapp/api/v1/task"
url_timeRecord = weclapp_base_url + "/webapp/api/v1/timeRecord"


parser = argparse.ArgumentParser(
    description="Add Data to weclapp",
    epilog="""
    ***Necessary Variables:
        Set Authentication-Token as Environment-Variable
"""
)

parser.add_argument("--json",
                    type=str,
                    help="JSON-filepath which should be used",
                    required=True)

parser.add_argument("--full_sync",
                    action="store_true",
                    help="Deletes timerecords before adding",
                    )

parser.add_argument("--inc_sync",
                    action="store_true",
                    help="Update duration of timerecords if the rest is the same")

args = parser.parse_args()

headers = {
    "AuthenticationToken": auth_token,
    "Content-Type": "application/json"
}

#--------------------CREATE USER----------------------------------------------------------

def read_json_file(file_path):
    try:
        with open(file_path, 'r') as file:
            data = json.load(file)
            return data
    except FileNotFoundError:
        print(f"Datei '{file_path}' nicht gefunden.")
        return None
    except json.JSONDecodeError:
        print(f"Die Datei '{file_path}' enthält ungültiges JSON.")
        return None
    
def create_user(user):
    response = requests.post(url_user, headers=headers, data=json.dumps(user))
    if response.status_code == 201:
        print("Successfully created user on weclapp!")
    elif response.status_code == 400:
        print(f"Email address '{user['email']}' already in use.")
    else:
        print(f"Error (User-creation failed): {response.status_code}")

def create_or_update_users(data):
    for raw in data:
        first_name, last_name = raw["user_name"].split(" ", 1)
        e_mail = raw["user_mail"]
        response = requests.post(url_user, headers=headers, data=json.dumps({
            "firstName": first_name,
            "lastName": last_name,
            "email": e_mail
        }))
        if response.status_code == 201:
            print(f"Successfully created user with email '{e_mail}'")
        elif response.status_code == 400:
            response = requests.get(url_user, headers=headers)
            for raw in response.json()["result"]:
                if raw["email"] == e_mail and (raw["firstName"] != first_name or raw["lastName"] != last_name):
                    response = requests.put(f"{url_user}/id/{raw['id']}", headers=headers, data=json.dumps({
                            "firstName": first_name,
                            "lastName": last_name,
                            "email": e_mail,
                            "status": raw["status"],
                            "userRoles": raw["userRoles"]
                        }))
                    print(f"Successfully updated username '{first_name} {last_name}'")
                    break             
        else:
            print(f"Error (User-creation/update failed): {response.status_code}")

#--------------------CREATE TIME RECORD-------------------------------------------------

def retrieve_user_by_email(email):
    response = requests.get(url_user, headers=headers)
    for raw in response.json()["result"]:
        if email == raw["email"]:
            return raw
    raise Exception(f'No user with email {email}!')    

def retrieve_sales_order_by_order_number(order_number):
    response = requests.get(url_salesOrder, headers=headers)
    for raw in response.json()["result"]:
        if order_number == raw["orderNumber"]:
            return raw
    raise Exception(f'No sales order with number {order_number}!')

def retrieve_tasks():
    response = requests.get(url_tasks, headers=headers)
    tasks = {}
    for raw in response.json()["result"]:
        tasks[raw["id"]] = raw
    return tasks

def ensure_assignee(task, user_id):
    assignee_exists = False
    for assignee in task["assignees"]:
        if user_id == assignee["userId"]:
            assignee_exists = True
    if not assignee_exists:
        new_assignee = {"userId": user_id}
        task["assignees"].append(new_assignee)
        response = requests.put(f"{url_tasks}/id/{task['id']}", headers=headers, data=json.dumps(task))

def create_time_record(order_number, pos, user_email, start_date, duration, description):
    user = retrieve_user_by_email(user_email)
    user_id = user["id"]
    sales_order = retrieve_sales_order_by_order_number(order_number)
    tasks = retrieve_tasks()

    for order_item in sales_order["orderItems"]:
        if order_item["positionNumber"] == pos:
            task_id = order_item["tasks"][0]["id"] # es gibt in der Regel nur einen Task pro Auftrag
            task = tasks[task_id]
            ensure_assignee(task, user_id)

            new_time_record = {
                "taskId": task_id,
                "userId": user_id,
                "startDate": start_date, # int(datetime.strptime(start_date, "%Y-%m-%d %H:%M").timestamp() * 1000),
                "durationSeconds": duration,
                "description": description
            }

            response = requests.post(url_timeRecord, headers=headers, data=json.dumps(new_time_record))
            if response.status_code == 201:
                print(f"Successfully added timeRecord '{description}'!")
            else: 
                print(f"Unsuccessful: '{description}' Status-Code: {response.status_code}")
            return

#---------------------DELETE TIMERECORD-------------------------------------------------------------------
        
def retrieve_time_records():
    response = requests.get(url_timeRecord, headers=headers)
    time_records = {}
    for raw in response.json()["result"]:
        time_records[raw["id"]] = raw
    return time_records

def delete_all_time_records_by_date(date, order_number, position):
    time_records = retrieve_time_records()
    for time_record in time_records.values():
        start_date_of_time_record = datetime.utcfromtimestamp(time_record["startDate"]/1000).strftime("%Y-%m-%d")
        task_id = time_record["taskId"]
        sales_order = retrieve_sales_order_by_order_number(order_number)

        for order_item in sales_order["orderItems"]:
            if order_item["positionNumber"] == position:
                for task in order_item["tasks"]:
                    if start_date_of_time_record == date and task["id"] == task_id:
                        response = requests.delete(f"{url_timeRecord}/id/{time_record['id']}", headers=headers)
                        if response.status_code == 204:
                            print(f"Successfully deleted timeRecord on date {datetime.utcfromtimestamp(time_record['startDate']/1000).strftime('%Y-%m-%d %H:%M')}")
                        else:
                            print(f"Deletion unsuccessful of timeRecord on date {datetime.utcfromtimestamp(time_record['startDate']/1000).strftime('%Y-%m-%d %H:%M')}")

def delete_time_record_by_date_ordernumber_position_description_duration(date, order_number, position, user_mail, description, duration):
    time_records = retrieve_time_records()
    sales_order = retrieve_sales_order_by_order_number(order_number)
    for time_record in time_records.values():
        if time_record["startDate"] == date and time_record["userUsername"] == user_mail and time_record["description"] == description and time_record["durationSeconds"] == duration:
            task_id = time_record["taskId"]
            for order_item in sales_order["orderItems"]:
                if order_item["positionNumber"] == position:
                    for task in order_item["tasks"]:
                        if task["id"] == task_id:
                            response = requests.delete(f"{url_timeRecord}/id/{time_record['id']}", headers=headers)
                            if response.status_code == 204:
                                print(f"Successfully deleted timeRecord on date {datetime.utcfromtimestamp(time_record['startDate']/1000).strftime('%Y-%m-%d %H:%M')}")
                            else:
                                print(f"Deletion unsuccessful of timeRecord on date {datetime.utcfromtimestamp(time_record['startDate']/1000).strftime('%Y-%m-%d %H:%M')}")

def delete_time_record_from_to_date_from_json(data):
    #Date-Range der Json-Daten ermitteln (von, bis)
    first_date, last_date = 9999999999999, 0
    for raw in data:
        first_date = min(first_date, raw["day"])
        last_date = max(last_date, raw["day"] + 32400000) # 32400000 ms = 9 h zusätzlich
    # In weclapp alle Zeitbuchungen im zeitraum von - bis löschen
    time_records = retrieve_time_records()
    for time_record in time_records.values():
        if first_date <= time_record["startDate"] <= last_date:
            response = requests.delete(f"{url_timeRecord}/id/{time_record['id']}", headers=headers)
            if response.status_code == 204:
                print(f"Successfully deleted timeRecord on date {datetime.utcfromtimestamp(time_record['startDate']/1000).strftime('%Y-%m-%d %H:%M')}")
            else:
                print(f"Deletion unsuccessful of timeRecord on date {datetime.utcfromtimestamp(time_record['startDate']/1000).strftime('%Y-%m-%d %H:%M')}")

#-----------------------------UPDATE TIME RECORD ------------------------------------------------------------------------------

def update_time_record(date, order_number, position, user_mail, description, new_duration):
    time_records = retrieve_time_records()
    sales_order = retrieve_sales_order_by_order_number(order_number)
    for time_record in time_records.values():
        if time_record["startDate"] == date and time_record["userUsername"] == user_mail and time_record["description"] == description:
            task_id = time_record["taskId"]
            for order_item in sales_order["orderItems"]:
                if order_item["positionNumber"] == position:
                    for task in order_item["tasks"]:
                        if task["id"] == task_id:
                            if time_record["durationSeconds"] != new_duration:
                                time_record["durationSeconds"] = new_duration # <------------- NEUE DURATION
                                response = requests.put(f"{url_timeRecord}/id/{time_record['id']}", headers=headers, data=json.dumps(time_record))
                                if response.status_code == 200:
                                    print(f"Successfully updated timeRecord '{time_record['description']}' {datetime.utcfromtimestamp(time_record['startDate']/1000).strftime('%Y-%m-%d %H:%M')}")
                                else:
                                    print(f"Update unsucessful of timeRecord on date '{time_record['description']}' {datetime.utcfromtimestamp(time_record['startDate']/1000).strftime('%Y-%m-%d %H:%M')}")
                            
#---------------------------------CHECK TIME RECORD EXISTS?--------------------------------------------------------------------------------

def does_time_record_exist(date, order_number, position, user_mail, description):
    time_records = retrieve_time_records()
    sales_order = retrieve_sales_order_by_order_number(order_number)
    for time_record in time_records.values():
        if time_record["startDate"] == date and time_record["userUsername"] == user_mail and time_record["description"] == description:
            task_id = time_record["taskId"]
            for order_item in sales_order["orderItems"]:
                if order_item["positionNumber"] == position:
                    for task in order_item["tasks"]:
                        if task["id"] == task_id:
                            return True
    return False

# ------------------------- SYNC FORMS -------------------------------------------------------------------------------------------

def full_sync(data):
    # Alle Zeitbuchungen im Zeitraum von data löschen
    delete_time_record_from_to_date_from_json(data)
    # Alle vorhandenden Benutzer in Json ermitteln und in weclapp anlegen/updaten
    create_or_update_users(data)
    # Alle Zeitbuchungen aus Json durchgehen und in weclapp anlegen
    for raw in data:
        e_mail = raw["user_mail"]
        start_date = raw["day"] + 32400000 # 32400000 ms = 9 hours
        order_number, position = raw["erp_link"].split("-")

        position = int(position)
        duration = int(raw["duration"] / 1000) # duration in seconds
        description = raw["output_summary"]
        create_time_record(order_number, position, e_mail, start_date, duration, description)

def inc_sync(data):
    # Alle vorhandenen Benutzer in Json ermitteln und in weclapp anlegen/updaten
    create_or_update_users(data)
    for raw in data:
        e_mail = raw["user_mail"]
        start_date = raw["day"] + 32400000 # 32400000 ms = 9 hours
        order_number, position = raw["erp_link"].split("-")

        position = int(position)
        duration = int(raw["duration"] / 1000) # duration in seconds
        description = raw["output_summary"]

        # Wenn Buchung (nicht) existiert ...
        if does_time_record_exist(start_date, order_number, position, e_mail, description):
            # update duration
            update_time_record(start_date, order_number, position, e_mail, description, duration)
        else:
            # erstelle neues
            create_time_record(order_number, position, e_mail, start_date, duration, description)

# ----------------------- FINAL --------------------------------------------------------------------------------------


def weclapp_add_data(data):
    if args.full_sync and args.inc_sync:
        print("Choose only one sync!")
    elif args.full_sync:
        full_sync(data)
    elif args.inc_sync:
        inc_sync(data)
    else:
        print("Choose one sync!")


weclapp_add_data(read_json_file(args.json))