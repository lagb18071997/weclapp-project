#!/usr/bin/env python3

import dataclasses
from dataclasses import dataclass
from typing import Optional

import requests
import json

from datetime import datetime

from pprint import pprint

from weclapp_credentials import *


headers = {
    "AuthenticationToken": auth_token,
    "Content-Type": "application/json"
}


@dataclass(frozen=True)
class User:
    id: str
    firstName: str
    lastName: str
    email: str

    @staticmethod
    def from_dict(raw):
        return User(
            raw["id"],
            raw["firstName"],
            raw["lastName"],
            raw["email"])

    def to_dict(self):
        return dataclasses.asdict(self)


def create_user(user):
    response = requests.post(url_user, headers=headers, data=json.dumps(user.to_dict()))
    if response.status_code == 201:
        print("Successfully created user on weclapp!")
    else:
        print(f"Error (User-creation failed): {response.status_code}")

def retrieve_users():
    response = requests.get(url_user, headers=headers)
    users = []
    for raw in response.json()["result"]:
        users.append(User.from_dict(raw))
    return users

@dataclass(frozen=True)
class SalesOrder:
    id: str
    commission: str
    order_number: str
    customer_number: str
    status: str
    net_amount: str
    sales_order_payment_type: str
    order_items: Optional[list] = None

    @staticmethod
    def from_dict(raw, with_order_items=False):
        if not with_order_items:
            return SalesOrder(
                raw["id"],
                raw.get("commission", ""),
                raw["orderNumber"],
                raw["customerNumber"],
                raw["status"],
                raw["netAmount"],
                raw["salesOrderPaymentType"]
            )
        else:
            return SalesOrder(
                raw["id"],
                raw.get("commission", ""),
                raw["orderNumber"],
                raw["customerNumber"],
                raw["status"],
                raw["netAmount"],
                raw["salesOrderPaymentType"],
                list(OrderItem.from_dict(raw_order_item) for raw_order_item in raw["orderItems"])
            )

@dataclass(frozen=True)
class OrderItem:
    id: str
    article_number: str
    pos: int
    title: str
    tasks: list    
    time_records: list

    @staticmethod
    def from_dict(raw):
        task_ids = raw["tasks"]
        time_records = []
        response = requests.get(url_timeRecord, headers=headers)
        
        for task_id in task_ids:
            for raw_time_record in response.json()["result"]:
                if raw_time_record["taskId"] == task_id["id"]:
                    time_records.append(TimeRecord.from_dict(raw_time_record))

        return OrderItem(
            raw["id"],
            raw["articleNumber"],
            raw["positionNumber"],
            raw["title"],
            raw["tasks"],
            time_records
        )
    
@dataclass(frozen=True)
class TimeRecord:
    id: str
    billable: bool
    created_date: str
    duration: int
    start_date: str
    username: str

    @staticmethod
    def from_dict(raw):
        return TimeRecord(
            raw["id"],
            raw["billable"],
            datetime.utcfromtimestamp(raw["createdDate"]/1000).strftime("%Y-%m-%d %H:%M"),
            raw["durationSeconds"],
            datetime.utcfromtimestamp(raw["startDate"]/1000).strftime("%Y-%m-%d %H:%M"),
            raw["userUsername"]
        )

def retrieve_sales_orders():
    response = requests.get(url_salesOrder, headers=headers)
    sales_orders = {}
    for raw in response.json()["result"]:
        sales_orders[raw["id"]] = SalesOrder.from_dict(raw)
    return sales_orders

pprint(retrieve_sales_orders())

def retrieve_time_records_of_sales_order():
    response = requests.get(url_salesOrder, headers=headers)
    time_records = requests.get(url_timeRecord, headers=headers)
    sales_orders = {}
    for raw in response.json()["result"]:
        sales_orders[raw["id"]] = SalesOrder.from_dict(raw, with_order_items=True)
    return sales_orders

def retrieve_time_records_of_sales_order_by_order_number(order_number):
    response = requests.get(url_salesOrder, headers=headers)
    for raw in response.json()["result"]:
        if raw["orderNumber"] == order_number:
            return SalesOrder.from_dict(raw, with_order_items=True)
    raise Exception(f"No sales order with number {order_number}")

