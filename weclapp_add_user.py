#!/usr/bin/env python3

import dataclasses
from dataclasses import dataclass

import requests
import json

from pprint import pprint

import argparse

from weclapp_credentials import *

parser = argparse.ArgumentParser(
    description="Add new User to weclapp",
    epilog="""
    ***Necessary Variables:
        Set Authentication-Token as Environment-Variable
"""
)

parser.add_argument("--firstName",
                    type=str,
                    help="First name of new user",
                    required=True)
parser.add_argument("--lastName",
                    type=str,
                    help="Last name of new User",
                    required=True)
parser.add_argument("--email",
                    type=str,
                    help="E-Mail of new user",
                    required=True
)

args = parser.parse_args()

headers = {
    "AuthenticationToken": auth_token,
    "Content-Type": "application/json"
}

def create_user(user):
    response = requests.post(url_user, headers=headers, data=json.dumps(user))
    if response.status_code == 201:
        print("Successfully created user on weclapp!")
    else:
        print(f"Error (User-creation failed): {response.status_code}")

create_user({
    "firstName": args.firstName,
    "lastName": args.lastName,
    "email": args.email
}
)